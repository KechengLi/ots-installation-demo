
/**
 * Module dependencies.
 */

var express = require('express')
	, routes = require('./routes')
	, params = require('express-params')
	, upload = require('./routes/upload')
	, fs = require('fs')
	, spawn = require('child_process').spawn
	, settings = require('./routes/settings')
	, socket = require('./routes/socket')
	, RedisStore = require('connect-redis')(express);

var qs = require('qs');

var app = module.exports = express.createServer();

params.extend(app);

//Initialize database
// database.init();

// Configuration

app.configure(function(){
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.compiler({ src : __dirname + '/public', enable: ['less']}));
	app.use(express.cookieParser());
	app.use(express.session({ secret: "keyboard cat" }));
	app.use(app.router);
	app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
	app.use(express.errorHandler());
});

// Compatible

// Now less files with @import 'whatever.less' will work(https://github.com/senchalabs/connect/pull/174)
var TWITTER_BOOTSTRAP_PATH = './vendor/twitter/bootstrap/less';
express.compiler.compilers.less.compile = function(str, fn){
	try {
		var less = require('less');var parser = new less.Parser({paths: [TWITTER_BOOTSTRAP_PATH]});
		parser.parse(str, function(err, root){fn(err, root.toCSS());});
	} catch (err) {fn(err);}
}

// Routes

app.get('/', settings.index);
// app.get('/', settings.init);
app.get('/default', settings.index);
app.get('/norlex', settings.init);
app.get('/load', settings.load);
// app.get('/', upload.form);
app.post('/settings', settings.process);
app.post('/send', settings.send);
app.get('/chart?', settings.chart);
app.get('/svg?', settings.show);
app.get('/tsc?', settings.tscChart);
// Admin functions
app.post('/admin/save', settings.save);
app.get('/admin/login', settings.login);
app.get('/admin/logout', settings.logout);
app.post('/admin/authenticate', settings.authenticate);

app.listen(3000, function(){
	console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});

// // Starting OrientDB database as a child process
// var dbLog = fs.createWriteStream('./logs/db.log');
// function startDB() {

//   var orientDB = spawn("./orientdb-graphed-1.2.0/bin/server.sh", []);

//   orientDB.stdout.pipe(dbLog);

//   orientDB.on('exit', function(code, signal) {
//     console.log("Restarting Orient DB server")
//     startDB();
//   });
// }

// startDB();

// 

socket.listen(app);