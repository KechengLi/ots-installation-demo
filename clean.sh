node ./routes/dropDB.js &&
node ./routes/initialize.js &&
rm ./tmp/* &&
rm ./settings/*.tsc &&
rm ./public/json/*.json &&
rm ./public/images/svg/*.svg &&
rm ./public/images/datapack_images/* &&
rm ./public/manifest/*

