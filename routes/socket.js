/*
 * Author	: cnv
 * Filename	: socket.js
 * Created On	: Sat 02 Mar 2013 05:31:46 PM EST
 * 
*/

var fs = require('fs');
var qs = require('querystring');
var io = require("socket.io");
var default_datapacks = require('../app_settings/default_datapacks.json');
var TSCreator = require('./tscreator');

exports.listen = function(server){
	io.listen(server).sockets.on('connection', function(socket)  {
		console.log("socket io started and listening to the connection.");
		var files = {};
		var datapack_base = './tmp/';

		socket.on('Start', function(data) {
			socket.emit("Uploading");
			console.log(data);
			var name = data.name;
			files.name = {
			size : data.size,
			data : '',
			downloaded: 0,
			}
			var place = 0;

			try {
				fstat = fs.statSync(datapack_base + name);

				if (fstat.isFile()) {
					files.name.downloaded = fstat.size;
					place = fstat.size/524288;
				}
			} catch(err) {
			}

			console.log(name);
			fs.open(datapack_base + name, "a", 0755, function(err, fd){
				if(err) {
					console.log(err);
				} else {
					files.name.handler = fd;
						socket.emit('More', {place : place, percent : 0});
				}
			});
		});

		socket.on('Upload', function(data) {
			console.log("Upload the damn file");
			var name = data.name;
			files.name.downloaded += data.data.length;
			files.name.data += data.data;
			if(files.name.downloaded == files.name.size) {
				fs.write(files.name.handler, files.name.data, null
				,'Binary', function(err, data){});
				// generate the datapack after it is completely uploaded.
				socket.emit("Upload Success");
				generate(datapack_base + name, name);

			} else if (files.name.data.length > 10485760) {
				fs.write(files.name.handler, files.name.data, null, 'Binary', function (err, data) {
					files.name.data = "";
					var place = files.name.downloaded/ 524288;
					var percent = (files.name.downloaded/ files.name.size)*100;
					socket.emit('More', {place: place, percent: percent});
				});
			} else {
				var place = files.name.downloaded / 524288;
				var percent = (files.name.downloaded/ files.name.size)*100;
				socket.emit('More', {place: place, percent: percent});
			}
		});


		socket.on('Load', function(data) {
			generate("./default_datasets/" + data.name, data.name);
		});


		socket.on('Generate', function(data) {
			socket.emit("Generating");
			var req = new Request(qs.parse(data.settings));
			try {
				var tsc = new TSCreator();
				tsc.update(req, function(settings) {
					socket.emit('Generate Success', {id: settings.id});
				});	
			} catch (err) {
				socket.emit('Generate Error');
			}
		});


		function generate(datapack_tmp, datapack_name) {
			socket.emit("Generating");
			try {
				var tsc = new TSCreator();
				var settings_file = null;
				var datapacks = default_datapacks;
				for (var key in datapacks) {
					if (datapacks[key].file == datapack_name) {
						settings_file = datapacks[key].settings;
						break;
					}
				}
				console.log("------------------------");
				console.log(settings_file);
				tsc.loadDatapack(datapack_tmp, datapack_name, settings_file, function(settings) {
					socket.emit('Generate Success', {id: settings.id});
				});
			} catch (err) {
				socket.emit	('Generate Error');
			}
		}
	});
}


var Request = function(data) {
	this.data = data;
}

Request.prototype.param = function(key) {
	// body...
	return this.data[key];
};