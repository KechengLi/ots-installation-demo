var fs = require('fs');

function Manifest(id) {
	this.id = id;
	this.file_path = "./public/manifest/manifest" + id + ".appcache";
	this.cache = [];
	this.network = [];
	this.fallback = [];
	this.json_path = "/json/";
	this.svg_path = "/images/svg/";
}

module.exports = Manifest;

Manifest.prototype.addToCache = function(files) {
	// body...
	if (typeof files == "string") {
		this.cache.push(files);
	} else {
		if (typeof files == "object" && files.length > 0) {
			for (var i in files) {
				this.cache.push(files[i]);
			}
		}
	}
};

Manifest.prototype.addFolderToCache = function(path) {
	// body...
	var self = this;
	var files = fs.readdirSync(path);
	for (var i in files) {
		var file = path + "/" + files[i];
		if(fs.statSync(file).isDirectory()) {
			self.addFolderToCache(file);
		}else if(fs.statSync(file).isFile() && files[i] !== ".gitignore") {
			self.cache.push(file.replace('./public/','/'));
		}
	}
};

Manifest.prototype.addFolderAtFirstToCache = function(path) {
	// body...
	var self = this;
	var files = fs.readdirSync(path);
	for (var i in files) {
		var file = path + "/" + files[i];
		if(fs.statSync(file).isDirectory()) {
		}else if(fs.statSync(file).isFile() && files[i] !== ".gitignore") {
			self.cache.push(file.replace('./public/','/'));
		}
	}
};

Manifest.prototype.addIDToCache = function(id) {
	// body...
	var jsonFile = this.json_path + id + ".json";
	var jsonFileTimes = this.json_path + id + "-times.json";
	var svgFile = this.svg_path + id + ".svg";
	this.cache.push(jsonFile);
	this.cache.push(jsonFileTimes);
	this.cache.push(svgFile);
};

Manifest.prototype.addToNetwork = function(files) {
	// body...
	if (typeof files == "string") {
		this.network.push(files);
	} else {
		if (typeof files == "object" && files.length > 0) {
			for (var i in files) {
				this.network.push(files[i]);
			}
		}
	}
};


Manifest.prototype.addFolderToNetwork = function(path) {
	// body...
	var self = this;
	var files = fs.readdirSync(path);
	for (var i in files) {
		var file = path + "/" + files[i];
		if(fs.statSync(file).isDirectory()) {
			self.addFolderToNetwork(file);
		}else if(fs.statSync(file).isFile() && files[i] !== ".gitignore") {
			self.network.push(file.replace('./public/','/'));
		}
	}
};


Manifest.prototype.addToFallback = function(files) {
	// body...
	if (typeof files == "string") {
		this.fallback.push(files);
	} else {
		if (typeof files == "object" && files.length > 0) {
			for (var i in files) {
				this.fallback.push(files[i]);
			}
		}
	}
};


Manifest.prototype.generate = function() {
	// body...
	var self = this;
	var data = "CACHE MANIFEST\n";
	data += "#v1.0\n"; // adding the version number;

	for(var i in self.cache) {
		data += self.cache[i] + '\n';
	};	
	//wrtitng to cache list
	data += "CACHE:" + '\n';
	
	//wrtitng to network list
	data += "NETWORK:" + '\n';
	for(var i in self.network) {
		data += self.network[i] + '\n';
	};
	data += 'http://\n';

	
	//wrtitng to cache list
	data += "FALLBACK:"
	for(var i in self.fallback) {
		data += self.fallback[i] + '\n';
	};
	fs.writeFileSync(self.file_path, data);
};




