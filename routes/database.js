var OrientDB = require('orientdb');

/* updates delault keys with options */
function merge(defaults, options) {
	defaults = defaults || {};
	if (options && typeof options === 'object') {
		var keys = Object.keys(options);
		for (var i = 0, len = keys.length; i < len; i++) {
			var k = keys[i];
			if (options[k] !== undefined) defaults[k] = options[k];
		}
	}
	return defaults;
}

/* 
* Database object is used as a wrapper to support cascading operations 
* to orientdb database.
*/
function Database(options) {
	if(!(this instanceof Database)) return new Twitter(options);

	// This contains the default configuration setting to access the server.
	var defaults = {
		name: "tscreator",
		dbConfig: {user_name: "admin",user_password: "admin"},
		serverConfig: {host: 'localhost',port: 2424,user_name: "root",
			user_password: "73BFA847F5B0950B7523915734FE8076807FD1448BE10B6F4E97A67B5FD670F2"
		}
	};

	// This function overwrites the default opetion with the new options.
	this.options = merge(defaults,options);

	// console.log(this.options);

	// These are instance objects which are used for all the database related operations.
	this.server = new OrientDB.Server(this.options.serverConfig);
	this.db = new OrientDB.Db(this.options.name, this.server, this.options.dbConfig);
	this.graphDB = new OrientDB.GraphDb(this.options.name, this.server, this.options.dbConfig);

}

// exports the Database module
module.exports = Database;

// This function when called checks if the database exists
// or not and creates the database if it doesn't exists.
Database.prototype.create = function(callback) {
	var parent = this;	
	//connect to the server
	parent.server.connect(function(err, sessionId) {
		if(err) { 
			console.log(err); 
			return;
		}
		// checks if database exists
		parent.db.exist(function(err, result) {
			if(err) {
				console.log(err);
				return;
			}
			if (result) {
				console.log('Database "' + parent.db.databaseName +'" already exists');
				parent.db.close(function(err) {
					if (err) { console.log(err); return; }
					console.log("Closed connection");
					return;
				});
			} else {		
				parent.db.create(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Created Database: " + parent.db.databaseName);
					parent.createClass("log", callback);
				});
			}
		});
	});
};

// This function when called checks if the database exists
// or not and drops the database if it exists.

Database.prototype.drop = function() {
	var parent = this;	
	parent.server.connect(function(err, sessionId) {
		if(err) { 
			console.log(err); 
			return;
		}
		parent.db.exist(function(err, result) {
			if(err) {
				console.log(err);
				return;
			}
			if (result) {
				parent.db.drop(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Dropped Database: " + parent.db.databaseName);
					parent.db.close(function(err) {
						if (err) { console.log(err); return; }
						console.log("Closed connection");
						return;
					});
				});
			} else {
				console.log('Database "' + parent.db.databaseName +'" doesnot exists');
				parent.db.close(function(err) {
					if (err) { console.log(err); return; }
					console.log("Closed connection");
					return;
				});
			}
		});
	});
};


Database.prototype.createClass = function(name, callback) {
	var parent = this;	
	if (typeof name !== 'string') {
		console.log("Name of the class is undefined.");
		return;
	}
	parent.db.open(function(err) {
		if(err) {
			console.log(err);
			return;
		}

		console.log("Opened Database: " + parent.db.databaseName);

		parent.db.command("CREATE class " + name, function(err, results) {
			if(err) {
				console.log(err);
				parent.db.close(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Closed Database: " + parent.db.databaseName);
					return;
				});
				return;
			}
			console.log('Created Class: ' + name);
			if(typeof callback === "function") {
				callback(results);
			} else {
				parent.db.close(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Closed Database: " + parent.db.databaseName);
					return;
				});
			}
		});
	});
};

Database.prototype.dropClass = function() {
	// body...
};

Database.prototype.open = function(callback) {
	// body...
	var parent = this;	
	this.db.open(function (err) {
		if (err) {
			console.log(err);
			return;
		}
		console.log("Opened Database: " + parent.db.databaseName);
		if (typeof callback === "function") {
			callback();
		}
	});
};

Database.prototype.close = function(callback) {
	// body...
	var parent = this;	
	this.db.close(function (err) {
		if (err) {
			console.log(err);
			return;
		}
		console.log("Closed Database: " + parent.db.databaseName);
		if (typeof callback === "function") {
			callback();
		}
	});
};

Database.prototype.store = function(data,class_name,callback) {
	// body...
	var parent = this;	
	data["@class"] = class_name;
	parent.db.command("select from " + class_name + " where id=" + data.id, function(err, results) {
		if (err) {
			console.log(err);
			return;
		}
		if (results.length < 1) {
			console.log("Opened Database: " + parent.db.databaseName);
			parent.db.save(data, function(err,data) {
				if(err) {
					console.log(err);
					return;
				}
				console.log("Record saved in the database.");	
				if (typeof callback === "function") {
					callback();
				}
			});
		} else {
			console.log("Record already exists in the database.");
			if (typeof callback === "function") {
				callback(data);
			}
			return;	
		}
	});
};



Database.prototype.save = function(data,class_name, callback) {
	// body...
	var parent = this;	
	data["@class"] = class_name;
	// console.log(data);
	parent.db.open(function(err) {
		if(err) {
			console.log(err);
			parent.db.close(function(err) {
				if(err) {
					console.log(err);
					return;
				}
				console.log("Closed Database: " + parent.db.databaseName);
				return;
			});
			return;
		}

		parent.db.command("select from " + class_name + " where id=" + data.id, function(err, results) {
			if(err) {
				console.log(err);
				parent.db.close(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Closed Database: " + parent.db.databaseName);
					return;
				});
				return;
			}
			if (results.length < 1) {
				console.log("Opened Database: " + parent.db.databaseName);
				parent.db.save(data, function(err,data) {
					if(err) {
						console.log(err);
						return;
					}

					console.log('Document was successfully saved into class "' + class_name +'"');
					if (typeof callback === "function") {
						callback(data);
					}
				});
			} else {
				console.log("Record already exists in the database.");
				parent.db.close(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Closed Database: " + parent.db.databaseName);
					return;
				});
				return;	
			}
		});
	});
};

Database.prototype.command = function(command, callback) {
	var parent = this;
	parent.db.open(function(err) {
		if(err) {
			console.log(err);
			parent.db.close(function(err) {
				if(err) {
					console.log(err);
					return;
				}
				console.log("Closed Database: " + parent.db.databaseName);
				return;
			});
			return;
		}

		parent.db.command(command, function(err, results) {
			parent.db.close(function(err) {
				if(err) {
					console.log(err);
					return;
				}
				callback(results);
				console.log("Closed Database: " + parent.db.databaseName);
				return;
			});
		});
	});
};

Database.prototype.log = function(data, callback) {
	// body...
	var parent = this;	
	data["@class"] = "log";
	data.logged_at = new Date();
	// console.log(data);
	parent.db.open(function(err) {
		if(err) {
			console.log(err);
			return;
		}

		console.log("Opened Database: " + parent.db.databaseName);
		parent.db.save(data, function(err,data) {
			if(err) {
				console.log(err);
				return;
			}

			console.log('Document was successfully saved in log');
			if(typeof callback === "function") {
				callback(data);
			} else {
				parent.db.close(function(err) {
					if(err) {
						console.log(err);
						return;
					}
					console.log("Closed Database: " + parent.db.databaseName);
					return;
				});
			}
		});
	});
};