/* This file parses the settings xml file and generates the page */
var TSCreator = require('./tscreator');
var Manifest = require('./manifest');
var default_datapacks = require('../app_settings/default_datapacks.json');

var admin  = {
	user : "admin",
	password : "ehv#9a55w6",
};


var stylesheets = [
	"/stylesheets/jquery-ui-1.9.0.custom.css",
	"/stylesheets/style.css",
	"/stylesheets/custom.css",
];

var scripts = [
	'/javascripts/js/jquery-1.8.2.min.js',
	'/javascripts/js/jquery-ui-1.9.0.custom.min.js',
	'/socket.io/socket.io.js',
	'/javascripts/jstree/jquery.jstree.js',
	'/javascripts/bootstrap.min.js',
	// '/javascripts/appmanifest.js',
	'/javascripts/default_datapacks.js',
	'/javascripts/init_default_datapacks.js',
	'/javascripts/init-form.js',
	'/javascripts/tree.js',
	'/javascripts/chart.js',
	'/javascripts/tsc.js',
	'/javascripts/svg.js',
	'/javascripts/actions.js',
	'/javascripts/socket.js',
	'/javascripts/watermark.js',
];

var initscripts = [
	'/javascripts/js/jquery-1.8.2.min.js',
	'/javascripts/js/jquery-ui-1.9.0.custom.min.js',
	'/javascripts/bootstrap.min.js',
];

exports.index = function(req, res) {
	res.render("index", {
		title: "tscreator", stylesheets: stylesheets, scripts: scripts, default_datapacks : default_datapacks, is_admin : is_admin(req)
	});
}

exports.init = function(req, res) {
	res.redirect("/load?datapack=norlex");
}

exports.load = function(req, res) {
		var key = req.query.datapack;
		var datapacks = default_datapacks;
		var settings = null;
		if (key in datapacks){
			datapack_tmp = "./default_datasets/" + datapacks[key].file;
			datapack_name = datapacks[key].file;
			settings = datapacks[key].settings;
			var tsc = new TSCreator();
			try {
				tsc.loadDatapack(datapack_tmp, datapack_name, settings, function(settings) {
					res.redirect("/chart?id=" + settings.id);
				});
			} catch (err) {
				res.redirect("/");
			}
		}  else {
			res.redirect("/");
		}
}

exports.process = function(req, res) {
	var datapack_tmp = req.files.datapack.path;
	var datapack_name = req.files.datapack.name;
	if (req.files.datapack.size != 0 || req.files.datapack.name.length != 0) {
 		datapack_tmp = req.files.datapack.path;
		datapack_name = req.files.datapack.name;
	} else {
		datapack_tmp = "./default_datasets/" + req.param("default-datapack");
		datapack_name = req.param("default-datapack");
	}
	var settings = null;
	var tsc = new TSCreator();
	try {
		tsc.loadDatapack(datapack_tmp, datapack_name, settings, function(settings) {
			res.redirect("/chart?id=" + settings.id);
		});	
	} catch (err) {
		res.redirect("/");
	}
}

exports.chart = function(req,res) {
	var id = req.query["id"];
	res.render('chartsettings', {title: "Generate chart", id : 	id,
		scripts : scripts, stylesheets: stylesheets, default_datapacks : default_datapacks, datapack_id : getDefaultDatapackId(id), is_admin : is_admin(req)
	});
}

exports.show = function(req,res) {
	var chart = "/images/svg/" + req.query['id'] + ".svg";
	var settings = "/json/" + req.query['id'] + ".json";
	res.render('svg',{title: 'Generated Chart', chart: chart, id: req.query['id'],
		scripts : scripts, stylesheets: stylesheets, default_datapacks : default_datapacks, datapack_id : getDefaultDatapackId(req.query['id']), is_admin : is_admin(req)
	});
}

exports.tscChart = function(req,res) {
	var chart = "/images/svg/" + req.query['id'] + ".svg";
	var settings = "/json/" + req.query['id'] + ".json";
	res.render('svg', {
		title: 'Generated Chart', chart: chart, id: req.query['id'],scripts : scripts, 
		stylesheets: stylesheets, default_datapacks : default_datapacks, datapack_id : getDefaultDatapackId(req.query['id']), is_admin : is_admin(req)
	});
}

exports.send = function(req,res) {
	var tsc = new TSCreator();
	try {
		tsc.update(req, function(settings) {
			res.redirect("/chart?id=" + settings.id);
		});
	} catch (err) {
		res.redirect("/");
	}
}

exports.save = function(req,res) {
	var settings_id = req.param('settings_id');
	var tsc = new TSCreator();
	try {
		tsc.saveDefaultSettings(req, function(message) {
			res.redirect("/chart?id=" + settings_id);
		});
	} catch (err) {
		res.redirect("/chart?id=" + settings_id);
	}	
}

exports.login = function (req, res) {
	res.render("admin_login", {
		title: "tscreator", stylesheets: stylesheets, scripts: scripts,
	});
}

exports.authenticate = function (req, res) {
	req.session.user_id = req.param('user');
	req.session.password = req.param('password');
	res.redirect("/");
}

exports.logout = function (req, res) {
	delete req.session.user_id
	res.redirect("/");
}

function is_admin(req) {
	if(req.session != null) {
		console.log(req.session);
		if(req.session.user_id == admin.user && req.session.password == admin.password) {
			return true;
		}
	} 
	return false;
}

function getDefaultDatapackId(settings_id) {
	for (var datapack in default_datapacks) {
		if (default_datapacks[datapack].settings == settings_id) {
			return datapack;
		}
	}
	return null;
}	