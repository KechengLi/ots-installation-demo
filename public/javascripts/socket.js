$(document).ready(function() {
	var socket = io.connect(window.location.origin);
	var freader;
	var name;
	var selectedfile;

	$("input[name='datapack']").change(function(evt) {
		selectedfile = evt.target.files[0];
		console.log(selectedfile);
	});

	function StartUpload(){
		var default_datapack = $("input[name='default-datapack']").val();
		if(selectedfile != null){
			freader = new FileReader();
			name = selectedfile.name;
			freader.onload = function(evnt){
		 		socket.emit('Upload', { 'name' : name, data : evnt.target.result });
			}
			socket.emit('Start', { 'name' : name, 'size' : selectedfile.size });
		} else if (default_datapack != null || default_datapack != "") {
			socket.emit('Load', {name : default_datapack});
		}else {
			setChartErrorMessage('No file has been selected please select a file and reload.')
		}
	}

	socket.on('More', function (data){
		var place = data['place'] * 524288; //The Next Blocks Starting Position
		var newfile; //The Variable that will hold the new Block of Data
		newfile = selectedfile.slice(place, place + Math.min(524288, (selectedfile.size-place)));
		freader.readAsBinaryString(newfile);
		setProgressPercent(data.percent);
	});


	$("#datapack-form").submit(function() {
		showProgressWindow();
		console.log("Datapack submited");
		StartUpload();
		return false;
	});

	$(".chart-link").click(function() {
		var default_datapack = $(this).attr("file");
		showProgressWindow();
		socket.emit('Load', {name : default_datapack});
	});

	$("#settings-form").submit(function() {
		showProgressWindow();
		console.log("Settings submited");
		socket.emit('Generate', {settings : $(this).serialize()});
		return false;
	});

	socket.on('Uploading', function() {
		setProgressMessage("Uploading datapack ...");
		setProgressPercent(0);
		// unsetDangerProgressBar();
	});

	socket.on('Generating', function() {
		setProgressMessage("Generating chart ...");
		setProgressPercent(100);
		// setDangerProgressBar();
	});

	socket.on('Upload Success', function() {
		appendStatus("Datapack has been successfully uploaded.");
	});


	socket.on('Generate Error', function() {
		appendStatus("Application encountered error while generating chart.");
	});


	socket.on('Generate Success', function(data) {
		setProgressPercent(0);
		appendStatus("Chart has been successfully generated.");
		setProgressMessage("Loading chart ...");
		// unsetDangerProgressBar();

		var url = "/images/svg/" + data.id + ".svg";
		var req = new XMLHttpRequest();
		 
		req.addEventListener("progress", updateProgress, false);
		req.addEventListener("load", transferComplete, false);
		req.addEventListener("error", transferFailed, false);
		req.addEventListener("abort", transferCanceled, false);
		 
		req.open('GET', url, true);
		req.overrideMimeType("image/svg+xml");
		req.send();
		// progress on transfers from the server to the client (downloads)
		function updateProgress(evt) {
		  if (evt.lengthComputable) {
		    var percentComplete = Math.round(100*evt.loaded / evt.total);
		    setProgressPercent(percentComplete);
		  } else {
		    // Unable to compute progress information since the total size is unknown
		  }
		}
		 
		function transferComplete(evt) {
			appendStatus("Chart has been successfully loaded.");
			hideProgressBar();
			hideProgressWindow();
			var url = "chart?id=" + data.id;
			window.location.replace(url);
		}
		 
		function transferFailed(evt) {
			appendStatus("Failed to load the chart. Please try again.");
		}
		 
		function transferCanceled(evt) {
			appendStatus("Request for the chart has been canceled.");
		}	 
	});

	function setChartSuccessMessage(message) {
		$("#chart-alerts").html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Success! </h4><p>'+ message +'</div>');
	}

	function setChartWarningMessage(message) {
		$("#chart-alerts").html('<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Warning! </h4><p>'+ message +'</div>');
	}

	function setChartErrorMessage(message) {
		$("#chart-alerts").html('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Error! </h4><p>'+ message +'</div>');
	}

	function showProgressWindow() {
		$('#chart-progress').modal('show');
	}

	function hideProgressWindow() {
		$('#chart-progress').modal('hide');
	}

	function appendStatus(message) {
		$("#chart-status-messages").append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Success! </h4><p>'+ message +'</div>');
	}

	function setProgressMessage(message) {
		$("#chart-progress-bar-body").html('<div id="progress-message"></div>'
	  	+ '<div class="progress progress-striped active">'
		+ '<div class="bar" style="width: 0%;" id="chart-progress-bar"></div>'
		+ '</div>');
		$("#progress-message").text(message);
	}

	function setDangerProgressBar() {
		 $("#chart-progress-bar").addClass("progress-danger");
	}

	function unsetDangerProgressBar() {
		 $("#chart-progress-bar").removeClass("progress-danger");
	}

	function setProgressPercent(percent) {
		$("#chart-progress-bar").css({"width" : percent + "%"});
	}

	function hideProgressBar() {
		$("#chart-progress-bar-body").hide();
	}
});
