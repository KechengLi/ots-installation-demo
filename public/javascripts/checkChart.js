$(function() {
	var url = "/init";
	var req = new XMLHttpRequest();
	 
	req.addEventListener("progress", updateProgress, false);
	req.addEventListener("load", transferComplete, false);
	req.addEventListener("error", transferFailed, false);
	req.addEventListener("abort", transferCanceled, false);
	 
	req.open("GET", url, true);
	req.send();
	// progress on transfers from the server to the client (downloads)
	function updateProgress(evt) {
	  if (evt.lengthComputable) {
	    var percentComplete = 100*evt.loaded / evt.total;
    	$("#index-progress").removeClass("progress-danger");
    	$("#index-progress").addClass("progress-success");
    	$("#index-bar").css({"width" : percentComplete + "%"});
    	$("#index-progress-lable").text("Loaded " + percentComplete + "%");
	  } else {
	    // Unable to compute progress information since the total size is unknown
	  }
	}
	 
	function transferComplete(evt) {

	  window.location.replace(url);
	}
	 
	function transferFailed(evt) {
	  // alert("An error occurred while transferring the file.");
	  $("#index-progress").removeClass("progress-danger");
      $("#index-progress").addClass("progress-success");
	  window.location.replace(url);
	}
	 
	function transferCanceled(evt) {
	  alert("The transfer has been canceled by the user.");
	}
});