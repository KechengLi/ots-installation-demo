var default_datapacks = {
	norlex : {
		name : "Arctic & Centeral Canada",
		file : "Arctic and Central Canada GTS2012 Public.map",
		image :  "/images/tsc/arcticcanada.jpg"
	},
	arctic_canada : {
		name : "Offshore Norway",
		file : "NORLEX biostrat 26Apr2013.zip",
		image : null,
	}
};