$(document).ready(function() {
	wmark.init({
		/* config goes here */
		"position": "bottom-right", // default "bottom-right"
		"opacity": 10, // default 50
		"className": "watermark", // default "watermark"
		"path": "/images/watermark.gif"
	});
});

function showPopup( event, text, elemID, textID ) {
	$(".ui-dialog-content").dialog("close");
	var title = "Additional Information ...";
	var width, height;

	var htmlText = $('<div>' + text + '</div>')
	var text_elements = htmlText.children();

	for (var i=0; i<text_elements.length; i++) {
		if($(text_elements[i]).is('img')) {
			
			var image_name = $(text_elements[i]).attr("src").split('/').pop();

			$(text_elements[i]).attr('src', '/images/datapack_images/' + image_name);
			// $(text_elements[i]).addClass("watermark");


			// var serializer = new XMLSerializer();
			// var svg = document.getElementById('SVGobject').contentDocument
			// // var svg = document.getElementById('chartConetentFrame').contentDocument.getElementById('SVGobject').contentDocument;
			// // alert(image_name);
			// var image = svg.getElementById(image_name);
			// var name = image_name.split('.');
			
			// if(image == null) {
			// 	image_name = name[0] + "_90." + name[1];
			// 	image = svg.getElementById(image_name);
			// }


			// if(image == null) {
			// 	image_name = name[0] + "90." + name[1];
			// 	image = svg.getElementById(image_name);	
			// }

			// if(image == null) {
			// 	image_name = name[0] + " 90." + name[1];
			// 	image = svg.getElementById(image_name);	
			// }


			// if(image == null) {
			// 	image_name = name[0] + "_90.png";
			// 	image = svg.getElementById(image_name);	
			// }

			// if(image == null) {
			// 	image_name = name[0] + "90.png";
			// 	image = svg.getElementById(image_name);	
			// }

			// if(image == null) {
			// 	image_name = name[0] + " 90.png";
			// 	image = svg.getElementById(image_name);	
			// }

			// // alert(image_name);
			// if (image != null) 
			// {
			// 	var w = image.getAttribute('width');
			// 	var h = image.getAttribute('height');
			// 	var x = image.getAttribute('x');
			// 	var y = image.getAttribute('y');
			// 	var nx = 0;
			// 	var ny = 0;
			// 	var nw = w*3;
			// 	var nh = h*3;
			// 	var transform = image.getAttribute('transform');
			// 	var transformList = image.transform.baseVal;
			// 	var rotate = null;
			// 	var translate = null;
			// 	try {
			// 		var angle = transformList.getItem(1).angle;
			// 		if(angle == 270) {
			// 			nx = 0;
			// 			ny = Math.max(nw,nh);
			// 			nw = h*3;
			// 			nh = w*3;
			// 			rotate = 'rotate(270,0,0)';

			// 			translate = 'translate(' + nx + ',' + ny + ')';
			// 		}
			// 	} catch(err) {
			// 	}
			// 	var ns = Math.max(nw,nh);
			// 	var svgSize = Math.max(nw + 10,nh + 10);
			// 	image.setAttribute('width', ns);
			// 	image.setAttribute('height', ns);
			// 	image.setAttribute('x', 0);
			// 	image.setAttribute('y', 0);
			// 	image.setAttribute('transform', translate + ' ' + rotate);
			// 	var svgimage = '<br/><svg xmlns="http://www.w3.org/2000/svg"	width=' + svgSize + ' height=' + svgSize + ' version="1.1">'+ serializer.serializeToString(image) + '</svg><hr/>';
			// 	image.setAttribute('width', w);
			// 	image.setAttribute('height', h);
			// 	image.setAttribute('x', x);
			// 	image.setAttribute('y', y);
			// 	image.setAttribute('transform', transform);
				// console.log(serializer.serializeToString(text_elements[i]));
			// 	// text = text.replace(serializer.serializeToString(text_elements[i]), svgimage);
			// }
			// else {
			// 	// text = text.replace(serializer.serializeToString(text_elements[i]), '<br>');
			// }
		}
	}

	// Required for iPad... 
	var $dialog = htmlText;
	$dialog.find('a').wrap('<span class="popup-link" />');

	$dialog.find('a').unbind("click").click(newWindow);

	// var x = event.clientX - $(window).scrollLeft() + $("#SVGobject").position().left + 20;
	// var y = event.clientY - $(window).scrollTop() - $dialog.parent('.ui-dialog:first').outerHeight() + $("#SVGobject").position().top;
	wmark.init({
		/* config goes here */
		"position": "bottom-right", // default "bottom-right"
		"opacity": 10, // default 50
		"className": "watermark", // default "watermark"
		"path": "/images/watermark.gif"
	});
	$dialog.dialog({
		title: title,
		dialogClass:'dialog_box',
		width: $(window).width()*0.3,
		height: $(window).height()*0.9,
		position: [0,$(window).height()*0.1]
	});

	$dialog.effect("slide");
}

function newWindow( event )
{
  window.open($(this).attr('href'), '_blank').focus();
  
  return false;
}
