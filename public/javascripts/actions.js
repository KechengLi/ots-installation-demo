$(function() {
	$(".select-file").click(function() {
		var file = $(this).attr("file");
		$("input[name='default-datapack']").val(file);
	});

	$("input[name='datapack']").change(function() {
		$("input[name='default-datapack']").val($("input[name='datapack']").val());
	});

	$("#generate-link").click(function(){
		$("#generate").click();
	});

	$(".showchart").click(function(){
		var url = $(this).attr("link");
		window.location.replace(url);
	});

	$(".zin").tooltip({
		animation : true,
		placement : "bottom",
		title : "zoom in",
		trigger : 'hover'
	});

	$(".zout").tooltip({
		animation : true,
		placement : "bottom",
		title : "zoom out",
		trigger : 'hover'
	});

	$(".z100").tooltip({
		animation : true,
		placement : "bottom",
		title : "zoom to original size",
		trigger : 'hover'
	});

	$(".zfit").tooltip({
		animation : true,
		placement : "bottom",
		title : "zoom to fit the screen",
		trigger : 'hover'
	});

	$(".showline").tooltip({
		animation : true,
		placement : "bottom",
		title : "enable timeline on the chart",
		trigger : 'hover'
	});

	$(".showchart").tooltip({
		animation : true,
		placement : "bottom",
		title : "show chart only",
		trigger : 'hover'
	});

	$(".list-btn").tooltip({
		animation : true,
		placement : "bottom",
		title : "display the list of default datapacks",
		trigger : 'hover'
	});

	$(".load-btn").tooltip({
		animation : true,
		placement : "bottom",
		title : "load the selected datapack",
		trigger : 'hover'
	});

	$(".generate-btn").tooltip({
		animation : true,
		placement : "bottom",
		title : "generate chart",
		trigger : 'hover'
	});
});
