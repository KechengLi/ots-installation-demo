$(document).ready(function(){
	var filepath = "/json/" + $("#timeinterval").attr("settings") + "-times.json";
	$.getJSON(filepath, function(data) {
		// alert(JSON.stringify(data));
		var formarray = new Array();
		var formhtml = '<filedset>';
		formarray[0] = formhtml;
		var timedata = data.setting;
		for (var i = 0; i < timedata.length; i++) {
			var setting = timedata[i];
			if (setting.$.name == "topAge") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">';
				formhtml += '<span class="add-on span1">Top age</span>';
				var age = setting.setting[0]._ ;
				if (setting.setting.length > 1) { 
					age = setting.setting[1]._ ; 
				}
				formhtml +=	'<input class="span1" type="text" name="topAge" value=' + age + '>';
				formhtml += '</div></div>';
				formarray[1] = formhtml;
			} else if (setting.$.name == "baseAge") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">';
				formhtml += '<span class="add-on span1">Base age</span>'
				var age = setting.setting[0]._ ; 
				if (setting.setting.length > 1) {
					age = setting.setting[1]._ ; 
				} 
				formhtml += '<input class="span1" type="text" name="baseAge" value=' + age + '>';
				formhtml += '</div></div>';
				formarray[2] = formhtml;
			} else if (setting.$.name == "unitsPerMY") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">';
				formhtml += '<span class="add-on span1">Vertical scale</span>';
				var vertScale = Number(setting._)/30;
				formhtml += '<input class="span1" type="text" name="unitsPerMY" value=' + vertScale +'>';
				formhtml += '</div></div>';
				formarray[3] = formhtml;
			} else if (setting.$.name == "variableColors") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">';
				formhtml += '<span class="add-on span1">Color scheme</span>';
				formhtml += '<select class="span1" name="variableColors">';
				var scheme = setting._;
				if ( scheme == "USGS") {
					formhtml += '<option value="unesco">UNESCO</option>';
					formhtml += '<option value="usgs" selected>USGS</option>';
				} else {
					formhtml += '<option value="unesco" selected>UNESCO</option>';
					formhtml += '<option value="usgs">USGS</option>';
				}
				formhtml += '</select>';
				formhtml += '</div></div>';
				formarray[4] = formhtml;
			} else if (setting.$.name == "skipEmptyColumns") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">'; 
				if (setting._ == "true") {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="skipEmptyColumns" checked="checked">Skip empty columns</label>';
				} else {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="skipEmptyColumns">Skip empty columns</label>';
				}
				formhtml += '</div></div>';
				formarray[9] = formhtml;
			} else if (setting.$.name == "enChartLegend") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">'; 
				if (setting._ == "true") {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="enChartLegend" checked="checked">Enable chart legend</label>';
				} else {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="enChartLegend">Enable chart legend</label>';
				}
				formhtml += '</div></div>';
				formarray[7] = formhtml;
			} else if (setting.$.name == "enPriority") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">'; 
				if (setting._ == "true") { 
					formhtml += '<label class="checkbox"><input  type="checkbox" name="enPriority" checked="checked">Enable priority filtering</label>';
				} else {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="enPriority">Enable priority filtering</label>';
				}
				formhtml += '</div></div>';
				formarray[6] = formhtml;
			} else if (setting.$.name == "enHideBlockLable") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">'; 
				if (setting._ == "true") {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="enHideBlockLable" checked="checked">If crowded, hide block labels</label>';
				} else {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="enHideBlockLable">If crowded, hide block labels</label>';
				}
				formhtml += '</div></div>';
				formarray[8] = formhtml;
			} else if (setting.$.name == "doPopups") {
				formhtml = '<div class="controls"><div class="input-prepend input-pad">';	
				if (setting._ == "true") {
					formhtml += '<label class="checkbox"><input  type="checkbox" name="doPopups" checked="checked">Enable popups</label>';
				} else { 
					formhtml += '<label class="checkbox"><input  type="checkbox" name="doPopups">Enable popups</label>';
				}
				formhtml += '</div></div>';
				formarray[5] = formhtml;
			}
		} 
		formhtml = '</filedset>';
		formarray[12] = formhtml;
		$("#timeinterval").html(formarray.join(''));
	});
});