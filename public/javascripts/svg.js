$(document).ready(function() {

	var svg_elm = false;
	var elm;
	var ratio; //aspect ratio of chart
	var fullwidth; //original width of chart
	var hideTimeLine = true;
	
	elm = $("#SVGobject");
	if( $(elm).length )
	{
	  //Makes javascript check every half sec to see if SVG is loaded
	  var get_svg_id = setInterval(get_svg, 500);
	}
	else
	{
	  elm = $("#chartObject");
	  if( $(elm).length )
	  {
	    //Weird thing to make the onload work all the time
	    elm[0].hasloaded = false;
	    elm[0].onload = get_png;
	    if( elm[0].complete )
	      elm[0].onload();
	  }
	}

	if( $(elm).length )
	{
		$(elm).width("100%");
		$(elm).height("100%");
    	var fitwidth = $(elm).width();
		chart_resize(fitwidth);
	}

	$("a.showline").click( function() {

		var svg = document.getElementById("SVGobject");
		if( svg != null )
		{
			var timeLine = svg.getSVGDocument().getElementById("timeline");
			var timeLabel = svg.getSVGDocument().getElementById("TimeLineLabel");

			hideTimeLine = !hideTimeLine;
			if (timeLine && timeLabel) {
				timeLine.setAttribute("style", hideTimeLine ?  "stroke-opacity : 0;" :"stroke: red;stroke-opacity:0.5;");
				timeLabel.setAttribute("style",hideTimeLine ? "fill-opacity : 0;" :"font-size: 10;fill: red; fill-opacity:0.7;");
			}
		}

		return false;
	});

	function zoomFit() {
		$(elm).width("100%");
    	$(elm).height("100%");
    	var fitwidth = $(elm).width();
		chart_resize(fitwidth);

		return false;
	}
	$("a.zfit").click(zoomFit);

	$("a.z100").click( function() {
		chart_resize(fullwidth);

		return false;
	});

	$("a.zin").click( function() {
		chart_scale(1.1);

		return false;
	});

	$("a.zout").click( function() {
		chart_scale(0.9);

		return false;
	});

//	$(window).bind( "beforeunload", function() {
//		close_popup();
//	});
	
	//Finds full and fit widths in px for png
	function get_png()
	{
	    fullwidth = $(elm).width();
	    ratio = $(elm).height() / fullwidth;
	    $(elm).width("100%");
	    $(elm).height("100%");
	    var fitwidth = $(elm).width();
	    $(elm).width(fitwidth);
	    $(elm).height(fitwidth * ratio);
	    $(elm).removeClass("noshow"); //Makes chart show on page
	}
	
	//Finds full and fit widths in px for svg
	function get_svg()
  {
    var svg = document.getElementById('SVGobject');
    if( svg.contentDocument != null )
    { 
      svg = svg.contentDocument.getElementsByTagName('svg');

		 // Get viewbox here and check for undefined to fix IE from failing...
		 var viewbox = $(svg).attr("viewBox");
     if( svg != null && viewbox !== undefined )
     {
       clearInterval(get_svg_id);
       svg_elm = svg;
       viewbox = viewbox.split(/ /);
       fullwidth = parseInt(viewbox[2]);
       ratio = parseInt(viewbox[3]) / fullwidth;
	     fitwidth = $(elm).width();
       chart_resize(fitwidth);
       $(elm).removeClass("noshow"); //Makes chart show on page
       return;
     } 
    }
  }

  //Changes size by a scaling factor
  function chart_scale(scale)
  {
    if( $(elm).length )
    {
      var width = $(elm).width() * scale;
      chart_resize(width);
    }
  }

  //Makes the chart have a given width, and put all sizes in terms of px
  function chart_resize(width)
  {
    if( $(elm).length )
    {
      var height = width * ratio;
      $(elm).attr("height", height + "px");
      $(elm).attr("width", width + "px");
      $(elm).width(width);
      $(elm).height(height);
      if( $(svg_elm).length )
      {
        $(svg_elm).attr("height", height + "px");
        $(svg_elm).attr("width", width + "px");
      }
    }
  }

  $(".chart_options").mouseenter(function() {
  	$(".chart_options").removeClass('transparent_class');
  });

  $("#chartObject").mouseenter(function() {
  	$(".chart_options").addClass('transparent_class');
  });
  chart_resize(1000);
});