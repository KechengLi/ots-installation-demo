var first = false;
$(function () {
	var filepath = "/json/" + $("#columnsettings").attr("settings") + ".json";
	$("#columnsettings").jstree({
		"themes" : {
			"theme" : "default",
			"dots" :  true,
			"icons" : false
		},
		"core" : {
			"html_titles" : true
		},
		"json_data" : {
			"ajax" : {
				"url" : filepath
			}
		},
		"checkbox": {
			"two_state" : true, 
			"real_checkboxes": true,
			"real_checkboxes_names" : function(n){
				return[n[0].id,1];
			}
		},
		"plugins" : [ "json_data","checkbox", "themes","ui"],
		"ui" : {"select_limit" : 1},
	}).bind("loaded.jstree", function (event, data) {
		// you get two params - event & data - check the core docs for a detailed description
		$.jstree._reference($("#columnsettings")).open_node($("#columnsettings").find("li.jstree-last")[0],function(){;},true);
		$.jstree._reference($("#columnsettings")).open_node($("#columnsettings").find("li.jstree-last")[1],function(){;},true);
		var chart_title = $(document.evaluate('//*[@id="class_datastore.RootColumn:Chart_Title"]/a/text()', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue).text();
		if (chart_title != "") {
			$(".chart-label").text("Chart - " + chart_title);
		}	
	});
});

