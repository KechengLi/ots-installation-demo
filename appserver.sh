#!/bin/bash
#####################################################################
# This is a daemon to control tscreator_web application			#
#####################################################################

# Application Info
APP_PATH="/home/strat/tscreator_webapp"
APP_NAME="tscreator_webapp"
APP_DESC="This is a timescale creator web-based application running on nodejs."
APP_FILE="./app.js"

# PID info
PID_PATH="./pid"
PID_FILE="$PID_PATH/$APP_NAME.pid"

# Logs info
LOG_PATH="./logs"
LOG_FILE="$LOG_PATH/$APP_NAME.log"
ERR_FILE="$LOG_PATH/$APP_NAME.err"

# NODE Commands
NODE_PATH="./node-v0.10.5-linux-x64"
NODE="$NODE_PATH/bin/node"
NPM="$NODE_PATH/bin/npm"

# Nohup command
NOHUP="/usr/bin/nohup"

# Display port for the tsc server
DISPLAY=":25"

usage() {
	echo "Usage: `basename $0`: <start|stop|status|check|install>"
	exit 1
}

# start the application
start() {
	# check if pid file exists
	status
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ $PID -gt 0 ]; then
		echo "$APP_NAME was already started. PID: $PID"
		return $PID
	fi
	# Start the application in case the application was not started
	echo "Starting $APP_NAME ..."
	echo "Starting vncserver"
	# kill currently running vncserver
	vncserver -kill $DISPLAY > /dev/null &
	sleep 2
	vncserver $DISPLAY -name $APP_NAME &
	# wait for 5 sec
	sleep 2 
	DISPLAY=$DISPLAY $NOHUP $NODE $APP_FILE 1>$LOG_FILE 3>$ERR_FILE &
	PID=$(echo $!)
	# save the PID ot the file
	echo $PID
	if [ $PID -gt 0 ]; then
		echo $PID > $PID_FILE
		echo "Ok"
	fi
	cd $CURR_DIR
}

# stop the application
stop() {
	status
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ $PID -eq 0 ]; then
		return 0
	fi
	echo "Stopping $APP_NAME daemon ..."
	kill -9 $PID
	echo "Killing vncserver"
	vncserver -kill $DISPLAY > /dev/null &
	status
	if [ $PID -eq 0 ]; then
		echo "OK"
	else
		echo "FAIL"
	fi
	cd $CURR_DIR
}

# check if application is running or not if the application is not running restart it.
check() {
	status
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ $PID -eq 0 ]; then
		 # echo -e "A check was ran but there was no PID file. The script attempted to restart the server." | mail -s "$APP_NAME Failure `date`" "cnv1989@gmail.com"
		start
	fi
	cd $CURR_DIR
}

# get the status of the application
status() {
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ -e $PID_FILE ]; then
		PID=$(cat $PID_FILE)
		if [ "x$PID" = "x" ]; then
			PID=0
		fi
		
		if [  -z "`ps axf | grep ${PID} | grep -v grep`" ]; then
			echo "Process is dead but the PID file exists."
			echo "Deleting PID file."
			rm -rf $PID_FILE
			PID=0
		fi	
	else
		PID=0
	fi

	if [ $PID -gt 0 ]
	then
		echo "$APP_NAME daemon is running with PID: $PID"
	else
		echo "$APP_NAME daemon is NOT running."
	fi
	cd $CURR_DIR
	# if PID is greater than 0 then the app is running
	return $PID
}

# install all the dependent packages
install() {
	$NPM install -d
}

if [ "x$1" = "xstart" ]
then
	start
	exit 0
fi

if [ "x$1" = "xstop" ]
then
	stop
	exit 0
fi

if [ "x$1" = "xcheck" ]
then
	check
	exit 0
fi

if [ "x$1" = "xstatus" ]
then
	status
	exit $PID
fi

if [ "x$1" = "xinstall" ]
then
	install
	exit 0
fi

usage
