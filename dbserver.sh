	#!/bin/bash
#####################################################################
# This is a daemon to control tscreator_web database				#
#####################################################################
# Application Info
APP_PATH="/home/strat/tscreator_webapp"

# database info
DB_PATH="./orientdb-graphed-1.2.0"
DB_NAME="orientdb"

# database scripts
DB_SERVER="$DB_PATH/bin/server.sh"
DB_SHUTDOWN="$DB_PATH/bin/shutdown.sh"

# PID info
PID_PATH="./pid"
PID_FILE="$PID_PATH/$DB_NAME.pid"

# Logs info
LOG_PATH="./logs"
LOG_FILE="$LOG_PATH/$DB_NAME.log"
ERR_FILE="$LOG_PATH/$DB_NAME.err"

# Nohup command
NOHUP="/usr/bin/nohup"

usage() {
	echo "Usage: `basename $0`: <start|stop|status|check|restart>"
	exit 1
}

# start the database
start() {
	# check if pid file exists
	status
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ $PID -gt 0 ]; then
		echo "$DB_NAME was already started. PID: $PID"
		return $PID
	fi
	# Start the database in case the database was not started
	echo "Starting $DB_NAME ..."
	PID=$($NOHUP $DB_SERVER 1>$LOG_FILE 3>$ERR_FILE & echo $!)
	# save the PID ot the file
	echo $PID
	if [ $PID -gt 0 ]; then
		echo $PID > $PID_FILE
		echo "Ok"
	fi
	cd $CURR_DIR
}

# stop the database
stop() {
	status
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ $PID -eq 0 ]; then
		return 0
	fi
	echo "Stopping $DB_NAME daemon ..."
	PID=$($NOHUP $DB_SHUTDOWN 1>$LOG_FILE 3>$ERR_FILE)
	status
	if [ $PID -eq 0 ]; then
		echo "OK"
	else
		echo "FAIL"
	fi
	cd $CURR_DIR
}

# check if database is running or not if the database is not running restart it.
check() {
	status
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ $PID -eq 0 ]; then
		 # echo -e "A check was ran but there was no PID file. The script attempted to restart the server." | mail -s "$DB_NAME Failure `date`" "cnv1989@gmail.com"
		start
	fi
	cd $CURR_DIR
}

# get the status of the database
status() {
	CURR_DIR=$(pwd)
	cd $APP_PATH
	if [ -e $PID_FILE ]; then
		PID=$(cat $PID_FILE)
		if [ "x$PID" = "x" ]; then
			PID=0
		fi

		if [  -z "`ps axf | grep ${PID} | grep -v grep`" ]; then
			echo "Process is dead but the PID file exists."
			echo "Deleting PID file."
			rm -rf $PID_FILE
			PID=0
		fi	
	else
		PID=0
	fi

	if [ $PID -gt 0 ]
	then
		echo "$DB_NAME daemon is running with PID: $PID"
	else
		echo "$DB_NAME daemon is NOT running."
	fi
	# if PID is greater than 0 then the app is running
	cd $CURR_DIR
	return $PID
}

if [ "x$1" = "xstart" ]
then
	start
	exit 0
fi

if [ "x$1" = "xstop" ]
then
	stop
	exit 0
fi

if [ "x$1" = "xcheck" ]
then
	check
	exit 0
fi

if [ "x$1" = "xstatus" ]
then
	status
	exit $PID
fi

usage
